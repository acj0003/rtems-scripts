#!/bin/bash -e

export PATH=$HOME/development/rtems/4.11.3/bin:$PATH
cd
cd development/rtems
mkdir kernel
cd kernel
git clone git://git.rtems.org/rtems.git rtems
cd rtems
git checkout tags/4.11.3
./bootstrap -c && ./bootstrap -p && \
    $HOME/development/rtems/rtems-source-builder/source-builder/sb-bootstrap
cd ..
mkdir arm_builds
cd arm_builds
$HOME/development/rtems/kernel/rtems/configure \
    --prefix=$HOME/development/rtems/4.11.3  --enable-tests \
    --target=arm-rtems4.11 --enable-rtemsbsp="xilinx_zynq_zedboard xilinx_zynq_a9_qemu"
gmake all
gmake install
