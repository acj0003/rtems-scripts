#!/bin/bash -e
cd
mkdir -p development/rtems
cd development/rtems
git clone git://git.rtems.org/rtems-source-builder.git
cd rtems-source-builder
git checkout 4.11.3
