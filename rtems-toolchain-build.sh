#!/bin/bash -e

cd ~/development/rtems/rtems-source-builder/rtems
../source-builder/sb-set-builder \
    --prefix=$HOME/development/rtems/4.11.3 \
    4.11/rtems-arm
